<?php

namespace Drupal\webform_donate_elements;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 * Provides a base for Webform donate composite elements.
 */
abstract class WebformDonateCompositeBase extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['multiple']['#access'] = FALSE;
    $form['element']['multiple_error']['#access'] = FALSE;
    $form['element']['multiple__header']['#access'] = FALSE;
    $form['element']['multiple__header_label']['#access'] = FALSE;

    return $form;
  }

}
