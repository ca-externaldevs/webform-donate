<?php

namespace Drupal\webform_donate_elements\Plugin\WebformElement;

use Drupal\Core\Form\FormState;
use Drupal\currency\Entity\Currency;
use Drupal\webform_donate_elements\WebformDonateCompositeBase;
use Drupal\webform_donate_elements\Element\WebformDonateAmount as WebformDonateAmountElement;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'donate amount' element.
 *
 * @WebformElement(
 *   id = "webform_donate_amount",
 *   label = @Translation("Donate amount"),
 *   category = @Translation("Webform Donate Amount"),
 *   description = @Translation("Provides a form element to set the donation amount. Requires the 'Webform Donate' form handler."),
 *   multiline = FALSE,
 *   composite = TRUE,
 *   states_wrapper = FALSE,
 * )
 */
class WebformDonateAmount extends WebformDonateCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    $default_properties = parent::getDefaultProperties();

    $default_properties['title'] = t('Amount');
    $default_properties['description'] = t('Donation selection element.');

    $default_properties['amount__placeholder'] = t('Enter amount');
    $default_properties['other_amount_button'] = FALSE;

    return $default_properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompositeElements() {
    return WebformDonateAmountElement::getCompositeElements([]);
  }

  /**
   * {@inheritdoc}
   */
  public function getInitializedCompositeElement(array $element, $composite_key = NULL) {
    $form_state = new FormState();
    $form_completed = [];
    return WebformDonateAmountElement::processWebformComposite($element, $form_state, $form_completed);
  }

  /**
   * Build the composite elements settings table.
   *
   * @return array
   *   A renderable array container the composite elements settings table.
   */
  protected function buildCompositeElementsTable() {
    $element = parent::buildCompositeElementsTable();

    $element['amount']['title_and_description']['data']['amount__placeholder']['#placeholder'] = $this->t('Enter other amount placeholder...');

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($value['currency']);
    $amount = $currency ? (string) $currency->formatAmount($value['amount']) : $value['amount'];

    $items = [];
    // TODO: Fix this correctly using labels from the payment handler.
    $frequency_value = $value['frequency'];
    if ($frequency_value == 'recurring') {
      $frequency_value = 'Monthly';
    }
    $items['donation_frequency'] = (string) $this->t('<em>Frequency:</em> @value', ['@value' => $frequency_value]);
    $items['donation_amount'] = (string) $this->t('<em>Amount:</em> @value', ['@value' => $amount]);
    $items['donation_currency'] = (string) $this->t('<em>Currency:</em> @value', ['@value' => $value['currency']]);

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($value['currency']);
    $amount = $currency ? (string) $currency->formatAmount($value['amount']) : $value['amount'];

    $items = [];
    $items['donation_frequency'] = (string) $this->t('Frequency: @value', ['@value' => $value['frequency']]);
    $items['donation_amount'] = (string) $this->t('Amount: @value', ['@value' => $amount]);
    $items['donation_currency'] = (string) $this->t('Currency: @value', ['@value' => $value['currency']]);
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(array &$element, WebformSubmissionInterface $webform_submission) {
    $data = $webform_submission->getData();
    $element_key = $element['#webform_key'];

    // Pick up data added by the donation webform handler.
    // todo: revisit this.
//    $data[$element_key]['currency'] = !empty($data['donation__currency']) ? $data['donation__currency'] : 'n/a';
//    $data[$element_key]['frequency'] = !empty($data['donation__frequency']) ? $data['donation__frequency'] : 'n/a';
//    $data[$element_key]['amount'] = !empty($data['donation__amount']) ? $data['donation__amount'] : 'n/a';
//    $webform_submission->setData($data);
  }

  /**
   * {@inheritdoc}
   */
  public function buildExportHeader(array $element, array $options) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildExportRecord(array $element, WebformSubmissionInterface $webform_submission, array $export_options) {
    return [];
  }

}
