<?php

namespace Drupal\webform_donate_elements\Plugin\WebformHandler;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\currency\Entity\Currency;
use Drupal\currency\FormHelperInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\Webform_donate_payment\Plugin\WebformHandler\DonatePaymentMethodsHandler;
use Drupal\media_entity\Entity\Media;

/**
 * Handler for donate elements.
 *
 * @WebformHandler(
 *   id = "donate_elements",
 *   label = @Translation("Donate Elements"),
 *   category = @Translation("Webform donate"),
 *   description = @Translation("Configure webform donate elements."),
 *   cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class DonateElementsHandler extends WebformHandlerBase {

  /**
   * The form helper.
   *
   * @var \Drupal\currency\FormHelperInterface
   */
  protected $formHelper;

  /**
   * Payment Methods handler
   *
   * @var \Drupal\webform_donate_payment\Plugin\WebformHandler\DonatePaymentMethodsHandler
   */
  protected $donatePaymentMethodsHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, FormHelperInterface $form_helper) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->formHelper = $form_helper;
    $this->donatePaymentMethodsHandler = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('currency.form_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaults = [
      'default_approach_code' => '',
      'currency' => 'GBP',
      'frequency_default' => NULL,
      'hero_image' => [
        'enabled' => FALSE,
        'image_reference' => NULL,
      ],
    ];
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment_methods = ($this->validPaymentMethodsHandler() ? $this->donatePaymentMethodsHandler->getPaymentMethods() : []);
    if (empty($payment_methods)) {
      $form['donate_payment_methods']['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No payment methods available. Please enable one on the site first (via Payment module), then configure the Webform Payment Methods handler.'),
      ];
      return $form;
    }

    $form['default_approach_code'] = [
      '#type' => 'textfield',
      '#title' => t('Default approach code'),
      '#default_value' => $this->configuration['default_approach_code'],
    ];

    foreach ($this->donatePaymentMethodsHandler->getPaymentFrequencies() as $payment_frequency_id => $payment_frequency) {
      $form[$payment_frequency_id] = [
        '#type' => 'details',
        '#title' => t('Payment frequency - ') . $payment_frequency['label'],
        '#open' => FALSE,
      ];

      $payment_frequency_payment_methods = [];
      foreach ($payment_methods as $payment_method_id => $method) {
        if ($this->donatePaymentMethodsHandler->getPaymentFrequencyStatus($payment_method_id, $payment_frequency_id)) {
          $payment_frequency_payment_methods[$payment_method_id] = $method;
        }
      }

      if (empty($payment_frequency_payment_methods)) {
        $form[$payment_frequency_id]['message'] = [
          '#type' => 'markup',
          '#markup' => $this->t('No payment methods available for :payment_type_label. Please enable one in the Donate Payment Methods Handler first.', [':payment_type_label' => $payment_frequency['label']]),
        ];
      }
      else {
        $form[$payment_frequency_id]['enabled'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enabled'),
          '#default_value' => $this->configuration[$payment_frequency_id]['enabled'],
        ];

        $form[$payment_frequency_id]['wrapper'] = [
          '#type' => 'fieldset',
        ];

        $form[$payment_frequency_id]['wrapper']['option_label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Option label'),
          '#default_value' => isset($this->configuration[$payment_frequency_id]['option_label']) ? $this->configuration[$payment_frequency_id]['option_label'] : $payment_frequency['default_option_label'],
        ];

        $form[$payment_frequency_id]['wrapper']['frequency_default'] = [
          '#type' => 'radio',
          '#title' => $this->t('Default'),
          '#return_value' => $payment_frequency_id,
          '#description' => $this->t('Set :payment_frequency as the default selected payment frequency', [':payment_frequency' => $payment_frequency['label']]),
          '#parents' => [
            'settings',
            'frequency_default',
          ],
          '#default_value' => $this->configuration['frequency_default'] == $payment_frequency_id ? $payment_frequency_id : NULL,
        ];

        $form[$payment_frequency_id]['wrapper']['payment_methods'] = [
          '#type' => 'details',
          '#title' => $this->t('Payment methods'),
          '#open' => FALSE,
        ];

        $default_payment_method = !empty($this->configuration[$payment_frequency_id]['default_payment_method'])
          ? $this->configuration[$payment_frequency_id]['default_payment_method']
          : reset($this->configuration[$payment_frequency_id]['payment_methods']);

        foreach ($payment_frequency_payment_methods as $payment_method_id => $payment_provider_definition) {
          $form[$payment_frequency_id]['wrapper']['payment_methods'][$payment_method_id] = [
            '#type' => 'fieldset',
            '#title' => $payment_provider_definition['label'],
            'enabled' => [
              '#markup' => $this->t('Enabled'),
            ],
            'default' => [
              '#type' => 'radio',
              '#title' => $this->t('Default'),
              '#return_value' => $payment_method_id,
              '#default_value' => $default_payment_method === $payment_method_id ? $payment_method_id : NULL,
              '#parents' => [
                'settings',
                $payment_frequency_id,
                'wrapper',
                'default_payment_method',
              ],
            ],
          ];
        }

        $form[$payment_frequency_id]['wrapper']['amounts'] = [
          '#type' => 'table',
          '#header' => [
            $this->t('Amount'),
            $this->t('Benefit'),
            $this->t('Default'),
          ],
        ];

        $amount = 6;

        for ($i = 0; $i < $amount; $i++) {
          $form[$payment_frequency_id]['wrapper']['amounts'][$i] = [
            'amount' => [
              '#type' => 'textfield',
              '#size' => 5,
            ],
            'benefit' => [
              '#type' => 'textfield',
            ],
            'default' => [
              '#type' => 'radio',
              '#parents' => [
                'settings',
                $payment_frequency_id,
                'wrapper',
                'default_amount',
              ],
              '#return_value' => $i,
              '#default_value' => $this->configuration[$payment_frequency_id]['default_amount'] == $i ? $i : NULL,
            ],
          ];
          if (isset($this->configuration[$payment_frequency_id]['amounts'][$i])) {
            $form[$payment_frequency_id]['wrapper']['amounts'][$i]['amount']['#default_value'] = $this->configuration[$payment_frequency_id]['amounts'][$i]['amount'];
            $form[$payment_frequency_id]['wrapper']['amounts'][$i]['benefit']['#default_value'] = $this->configuration[$payment_frequency_id]['amounts'][$i]['benefit'];
          }
        }

        // Allow other amount.
        // @TODO: grant access when JS and styling support non-other selection.
        $form[$payment_frequency_id]['wrapper']['allow_other_amount'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Allow other amount'),
          '#access' => FALSE,
          '#default_value' => isset($this->configuration[$payment_frequency_id]['allow_other_amount']) ? $this->configuration[$payment_frequency_id]['allow_other_amount'] : TRUE,
        ];

        // Set minimum amount.
        $form[$payment_frequency_id]['wrapper']['minimum_amount'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Minimum other amount'),
          '#default_value' => isset($this->configuration[$payment_frequency_id]['minimum_amount']) ? $this->configuration[$payment_frequency_id]['minimum_amount'] : '2',
        ];

        $media = NULL;
        // print_r($this->configuration); exit;
        if (!empty($this->configuration[$payment_frequency_id]['hero_image']['image_reference'])) {
          $media = Media::load($this->configuration[$payment_frequency_id]['hero_image']['image_reference']);
        }

        $form[$payment_frequency_id]['wrapper']['hero_image'] = [
          '#type' => 'details',
          '#title' => 'Donate element hero image',
          '#open' => FALSE,
        ];
        $form[$payment_frequency_id]['wrapper']['hero_image']['enabled'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enabled'),
          '#default_value' => $this->configuration[$payment_frequency_id]['hero_image']['enabled'],
          '#description' => t('Displays a hero image for one off payments in the donate widget form.'),
        ];
        $form[$payment_frequency_id]['wrapper']['hero_image']['image_reference'] = [
          '#title' => $this->t('Hero image'),
          '#type' => 'entity_autocomplete',
          '#target_type' => 'media',
          '#default_value' => $media,
          '#description' => t('Enter the name of the media image to display.'),
        ];
        $form[$payment_frequency_id]['wrapper']['hero_image']['image_style'] = [
          '#title' => $this->t('Image style'),
          '#type' => 'select',
          '#default_value' => $this->configuration[$payment_frequency_id]['hero_image']['image_style'],
          '#options' => image_style_options(FALSE),
          '#description' => t('Select display style of media image.'),
        ];
      }
    }

    $form['currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['currency'],
      '#options' => $this->formHelper->getCurrencyOptions(),
    ];

    // TODO: Update the hero image feature to use core media module.

    $form['currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['currency'],
      '#options' => $this->formHelper->getCurrencyOptions(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['frequency_default'] = $values['frequency_default'];
    $this->configuration['currency'] = $values['currency'];
    $this->configuration['default_approach_code'] = $values['default_approach_code'];
    $payment_frequencies = ($this->validPaymentMethodsHandler() ? $this->donatePaymentMethodsHandler->getPaymentFrequencies() : []);
    foreach ($payment_frequencies as $frequency_name => $frequency) {
      if (isset($values[$frequency_name])) {
        $enabled_methods = array_filter($values[$frequency_name]['wrapper']['payment_methods'], function ($value) {
          return !empty($value['enabled']);
        });

        $this->configuration[$frequency_name] = $values[$frequency_name]['wrapper'];
        $this->configuration[$frequency_name]['payment_methods'] = array_keys($enabled_methods);
        $this->configuration[$frequency_name]['enabled'] = $values[$frequency_name]['enabled'];
        $this->configuration[$frequency_name]['hero_image']['enabled'] = $values[$frequency_name]['wrapper']['hero_image']['enabled'];
        $this->configuration[$frequency_name]['hero_image']['image_reference'] = $values[$frequency_name]['wrapper']['hero_image']['image_reference'];
        $this->configuration[$frequency_name]['hero_image']['image_style'] = $values[$frequency_name]['wrapper']['hero_image']['image_style'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValues();

    $payment_frequencies = ($this->validPaymentMethodsHandler() ? $this->donatePaymentMethodsHandler->getPaymentFrequencies() : []);
    foreach ($payment_frequencies as $frequency_name => $frequency) {
      if ($values[$frequency_name]['hero_image']['enabled']) {

        if (empty($values[$frequency_name]['hero_image']['image_reference'])) {
          $form_state->setErrorByName("[$frequency_name][hero_image][image_reference]", t('Missing media image name which is required when hero image enabled.'));
        }
      }
    }
  }

  /**
   * Get all the payment provider plugins.
   *
   * @param string $type
   *   Either all, single, or recurring.
   *
   * @return \Drupal\webform_donate_elements\PaymentProviderInterface[]
   *   Initialised PaymentProvider plugins.
   */
  //    public function getPaymentProviders($type = NULL)
  //    {
  //        $payment_providers = [];
  //        $payment_methods = $this->paymentProviderPluginManager->getDefinitions();
  //        foreach ($payment_methods as $plugin_id => $payment_provider_definition) {
  //            if (empty($type) || $payment_provider_definition['type'] == $type || $payment_provider_definition['type'] == 'all') {
  //                $payment_providers[] = $this->paymentProviderPluginManager->createInstance($plugin_id);
  //            }
  //        }
  //
  //        return $payment_providers;
  //    }

  /**
   * Placeholder method for gettings frequency/amounts settings.
   *
   * @todo replace with perhaps a trait for donation configuration.
   *
   * @return array
   *   An array of amounts by frequency.
   */
  public function getAmounts() {
    $amounts_full = [];
    $currency = Currency::load($this->configuration['currency']);
    $payment_frequencies = ($this->validPaymentMethodsHandler() ? $this->donatePaymentMethodsHandler->getPaymentFrequencies() : []);
    foreach ($payment_frequencies as $payment_frequency_id => $payment_frequency) {
      if (!empty($this->configuration[$payment_frequency_id]['enabled'])) {
        $amounts = [];
        foreach ($this->configuration[$payment_frequency_id]['amounts'] as $amount_details) {
          if (!empty($amount_details['amount'])) {
            $amounts[$amount_details['amount']] = [
              'benefit' => $amount_details['benefit'],
              // TODO: Use formatter from currency module.
              'label' => $currency->getSign() . $amount_details['amount'],
            ];
          }
        }

        $amounts_full[$payment_frequency_id] = [
          'label' => !empty($this->configuration[$payment_frequency_id]['option_label']) ? $this->configuration[$payment_frequency_id]['option_label'] : $payment_frequency['label'],
          'amounts' => $amounts,
          'payment_methods' => $this->configuration[$payment_frequency_id]['payment_methods'],
          'allow_other_amount' => TRUE,
          'minimum_amount' => !empty($this->configuration[$payment_frequency_id]['minimum_amount']) ? $this->configuration[$payment_frequency_id]['minimum_amount'] : 0,
        ];
      }
    }
    return $amounts_full;
  }

  /**
   * Get defaults.
   *
   * @return array
   *   Array of defaults.
   */
  public function getAmountDefaults() {
    $amount_defaults = [];

    $payment_frequencies = ($this->validPaymentMethodsHandler() ? $this->donatePaymentMethodsHandler->getPaymentFrequencies() : []);;

    if (!empty($this->configuration['frequency_default'])) {
      $amount_defaults['frequency_default'] = $this->configuration['frequency_default'];
    }
    else {
      $amount_defaults['frequency_default'] = key($payment_frequencies);
    }

    foreach ($payment_frequencies as $payment_frequency_id => $payment_frequency) {
      $frequency_config = $this->configuration[$payment_frequency_id];

      // Get the default amount index.
      $amount_index = isset($frequency_config['default_amount']) ? $frequency_config['default_amount'] : key($frequency_config['amounts']);

      // Get the default amount key from the index.
      if (isset($frequency_config['amounts'][$amount_index]['amount'])) {
        $amount_defaults[$payment_frequency_id]['default_amount'] = $frequency_config['amounts'][$amount_index]['amount'];
      }
      else {
        // Set this incase no payment providers enabled.
        $amount_defaults[$payment_frequency_id]['default_amount'] = 0;
      }

      // Method does not use index so just set default to value.
      $amount_defaults[$payment_frequency_id]['default_payment_method'] = !empty($frequency_config['default_payment_method']) ? $frequency_config['default_payment_method'] : reset($frequency_config['payment_methods']);
    }

    return $amount_defaults;
  }

  /**
   * Get defaults with updated form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Array of defaults.
   */
  public function getAmountDefaultState(FormStateInterface $form_state) {
    // Get default options.
    $amount_defaults_all = $this->getAmountDefaults();

    // Attempt to get user selected options from form_state.
    $selected_frequency = $form_state->get('frequency');
    $selected_amount = $form_state->get('amount');
    $selected_payment_method = $form_state->get('payment_method');

    // Determine default form values based on availability of user selection.
    $default_frequency = $selected_frequency ? $selected_frequency : $amount_defaults_all['frequency_default'];
    $default_amount = $selected_amount ? $selected_amount : $amount_defaults_all[$default_frequency]['default_amount'];
    $default_payment_method = $selected_payment_method ? $selected_payment_method : $amount_defaults_all[$default_frequency]['default_payment_method'];

    // Override defaults with user selections.
    $amount_defaults_all['frequency_default'] = $default_frequency;
    $amount_defaults_all[$default_frequency]['default_amount'] = $default_amount;
    $amount_defaults_all[$default_frequency]['$default_payment_method'] = $default_payment_method;
    $amount_defaults_all['currency'] = $this->getCurrency();

    return $amount_defaults_all;
  }

  /**
   * Get component values
   *
   * @param string $component_name
   *   address element name
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Array of address values.
   */
  function getAddressValues($component_name, FormStateInterface $form_state) {
    $component = $form_state->getValue($component_name);
    $component_values = [];
    if (!empty($component)) {
      foreach ($component as $key => $line) {
        $component_values[$key] = $line;
      }
    }
    return $component_values;
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $form['#attached']['library'][] = 'webform_donate_elements/webform_donate_elements';
    $form['#attached']['drupalSettings']['webformDonateElements']['amounts'] = $this->getAmounts();
    $form['#attached']['drupalSettings']['webformDonateElements']['amount_defaults'] = $this->getAmountDefaultState($form_state);

    // todo: find method of assigning component values to personal details without hard coding.
    $form['#attached']['drupalSettings']['webformDonateElements']['personalDetails']['firstname'] = $form_state->getValue('firstname');
    $form['#attached']['drupalSettings']['webformDonateElements']['personalDetails']['lastname'] = $form_state->getValue('lastname');
    $form['#attached']['drupalSettings']['webformDonateElements']['personalDetails']['address'] = $this->getAddressValues('address', $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    parent::validateForm($form, $form_state, $webform_submission);
    $amount = $form_state->get('amount');
    $payment_frequency = $form_state->get('frequency');
    $payment_method = $form_state->get('payment_method');
    $payment_result = $form_state->get('payment_result');
    $payment_details = $form_state->get('payment_details');
    $currency = $this->getCurrency();

    if (!empty($amount) && !empty($payment_method)) {
      $payment_provider = $this->getPaymentProvider($payment_method);

      $params = [
        'amount' => $amount,
        'currency' => $this->configuration['currency'],
        'payment_details' => $payment_details,
        'payment_result' => $payment_result,
        'description' => 'Payment by' . $payment_method,
      ];

      // Check upper limit and set form error if the amount exceeds it.
      if (isset($payment_provider->pluginDefinition['paymentUpperLimit']) && $amount > $payment_provider->pluginDefinition['paymentUpperLimit']) {
        $form_state->setErrorByName($form['#payment_element_name'], t('We were unable to take the payment as your donation exceeds the maximum limit. Please decrease your request and try again.'));
        return;
      }

      try {
        $result = $payment_provider->processPayment($params);
      } catch (PaymentException $e) {
        $data['donation__status'] = 'failure';
        $webform_submission->setData($data);
        // Set a form error - please try again.
        $name = $form['#payment_element_name'];
        $form_state->setErrorByName($name, t('We were unable to take your payment. Please check your details and try again.'));
        return;
      }

      $data = $webform_submission->getData();

      $payment_data = [];
      $data_layer_data = [
        'payment_method' => $payment_method,
      ];

      // Set frequency and amount.
      $data_layer_data['amount'] = $amount;

      $payment_type_id = $data_layer_data['payment_type'] = $payment_provider->getPaymentType() ?: 'card';
      $payment_type = $this->getPaymentType($payment_type_id);

      // Add prefix with additional "donation__" prefix for webform elements
      // to identify data that needs to be exposed.
      $prefix = 'donation__' . $payment_type->getPrefix() . '_';

      $data[$prefix . 'currency'] = $payment_data['donation__currency'] = $data_layer_data['currency'] = $currency;
      $data[$prefix . 'frequency'] = $payment_data['donation__frequency'] = $data_layer_data['frequency'] = $payment_frequency;

      /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
      $currency = Currency::load($this->configuration['currency']);

      // Format the currency.
      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $amount_with_sign */
      $amount_with_sign = $currency->formatAmount($amount);
      $payment_data[$prefix . 'amount'] = $payment_data['donation__amount'] = $amount_with_sign->getArguments()['@amount'];

      /** @var \Drupal\Core\Datetime\DateFormatter $date_formatter */
      $date_formatter = \Drupal::service('date.formatter');
      $payment_data[$prefix . 'date'] = $data_layer_data['date'] = $date_formatter->format(time(), 'custom', 'd/m/Y');
      $payment_data[$prefix . 'fund_code'] = $data_layer_data['fund_code'] = $this->configuration['default_approach_code'];
      $payment_data[$prefix . 'package_code'] = $data_layer_data['package_code'] = $this->configuration['default_package_code'];

      $payment_data[$prefix . 'fulfillment_letter'] = 'website thank you';

      $payment_provider_data = $payment_provider->getPaymentData($params, $result);
      foreach ($payment_provider_data as $key => $value) {
        $payment_data[$prefix . $key] = $value;
      }

      $data = array_merge($data, $payment_data);

      $this->setDataLayer($data, $data_layer_data);

      $webform_submission->setData($data);
    }
  }

  /**
   * Set data layer data.
   *
   * @param array $data
   *   Data array to add data layer data to.
   * @param array $data_layer_data
   *   Data layer data.
   */
  protected function setDataLayer(array &$data, array $data_layer_data) {
    foreach ($data_layer_data as $key => $value) {
      $data['datalayer__' . $key] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDataLayer(WebformSubmissionInterface $webform_submission) {
    $data = $webform_submission->getData();
    $data_layer_data = [];

    foreach ($data as $key => $value) {
      if (strpos($key, 'datalayer__') === 0) {
        $data_layer_data_key = substr($key, 11);
        $data_layer_data[$data_layer_data_key] = $value;
      }
    }

    return $data_layer_data;
  }

  /**
   * Get donation status.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Webform submission.
   *
   * @return mixed
   *   Donation status.
   */
  public function getDonationStatus(WebformSubmissionInterface $webform_submission) {
    return $webform_submission->getData('donation__status');
  }

  /**
   * Populate the data layer.
   */
  protected function populateDataLayer($data) {
    if (\Drupal::moduleHandler()->moduleExists('datalayer')) {
      datalayer_add($data);
    }
  }

  /**
   * Get the currency that applies to this webform.
   *
   * @return string
   *   3 character currency code.
   */
  public function getCurrency() {
    return !empty($this->configuration['currency']) ? $this->configuration['currency'] : 'GBP';

  }

  /**
   * Format an amount using the donation currency.
   *
   * @param string $amount
   *   Unformatted amount.
   * @param string|null $symbol_code
   *   Either symbol or code.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Formatted amount.
   */
  public function formatAmount($amount, $symbol_code = NULL) {
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    $currency = Currency::load($this->getCurrency());

    // Format the currency.
    /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $amount_with_sign */
    $amount_with_sign = $currency->formatAmount($amount);

    if ($symbol_code == 'code') {
      return $amount_with_sign;
    }
    elseif ($symbol_code == 'symbol') {
      // @TODO: will not be needed if intl package enabled.
      return $this->t('@currency_sign@amount', $amount_with_sign->getArguments());
    }
    else {
      return $amount_with_sign->getArguments()['@amount'];
    }
  }

  /**
   * Helper to retrieve and check payment method handler is valid.
   *
   * @return bool
   */
  public function validPaymentMethodsHandler() {
    $handlers = $this->getWebform()->getHandlers('donate_payment_methods');
    if ($handlers->count() > 0) {
      $this->donatePaymentMethodsHandler = $handlers->getIterator()->current();
    }
    else {
      $this->donatePaymentMethodsHandler = FALSE;
    }
    $valid_handler = TRUE;
    return ($this->donatePaymentMethodsHandler !== FALSE ? $valid_handler : $this->donatePaymentMethodsHandler);
  }

}
