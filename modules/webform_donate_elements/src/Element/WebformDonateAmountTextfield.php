<?php

namespace Drupal\webform_donate_elements\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Number;

/**
 * Provides a textfield element with input mask and min.
 *
 * @FormElement("webform_donate_amount_textfield")
 */
class WebformDonateAmountTextfield extends Number {

  /**
   * {@inheritdoc}
   */
  protected static $type = 'number';

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $class = get_class($this);
    $info['#process'][] = [$class, 'processAmount'];
    return $info;
  }

  /**
   * Processes an 'other' element.
   *
   * See select list webform element for select list properties.
   *
   * @see \Drupal\Core\Render\Element\Select
   */
  public static function processAmount(&$element, FormStateInterface $form_state, &$complete_form) {
    if (!empty($element['#input_mask'])) {
      // See if the element mask is JSON by looking for 'name':, else assume it
      // is a mask pattern.
      $input_mask = $element['#input_mask'];
      if (preg_match("/^'[^']+'\s*:/", $input_mask)) {
        $element['#attributes']['data-inputmask'] = $input_mask;
      }
      else {
        $element['#attributes']['data-inputmask-mask'] = $input_mask;
      }


      // Assumes that this input mask is available.
      $element['#attributes']['class'][] = 'js-webform-input-mask';
      $element['#attached']['library'][] = 'webform/webform.element.inputmask';
    }

    $element['#step'] = '0.01';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderNumber($element) {
    $element = parent::preRenderNumber($element);

    // Let min through.
    Element::setAttributes($element, ['min']);

    return $element;
  }


}
