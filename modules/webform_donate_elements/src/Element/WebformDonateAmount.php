<?php

namespace Drupal\webform_donate_elements\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Entity\Webform;
use Drupal\media_entity\Entity\Media;

/**
 * Provides a webform element for an donations amount element.
 *
 * @FormElement("webform_donate_amount")
 */
class WebformDonateAmount extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements(array $element) {
    $elements = [];
    $elements['amount'] = [
      '#type' => 'number',
      '#title' => t('Amount'),
      '#attributes' => [
        'class' => ['webform_donate_elements_amount'],
      ],
    ];
    $elements['frequency'] = [
      '#type' => 'webform_buttons',
      '#title' => t('Frequency'),
      '#attributes' => [
        'class' => ['webform_donate_elements_frequency'],
      ],
      '#weight' => -1,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function processWebformComposite(&$element, FormStateInterface $form_state, &$complete_form) {
    parent::processWebformComposite($element, $form_state, $complete_form);

    if (!empty($element['#webform']) && $webform = Webform::load($element['#webform'])) {
      $handlers = $webform->getHandlers('donate_elements');
      if ($handlers->count() > 0) {
        /** @var \Drupal\webform_donate_elements\Plugin\WebformHandler\DonateElementsHandler $handler */
        $handler = $handlers->getIterator()->current();
        $amounts = $handler->getAmounts();
        $amount_defaults_all = $handler->getAmountDefaults();

        // Set frequency options.
        $element['frequency'] += WebformDonateAmount::getElementProperties($amounts);

        $element['frequency']['#default_value'] = $amount_defaults_all['frequency_default'];

        // If value already set, then need to preserve in replacement elements.
        if (!empty($element['#value']['frequency'])) {
          $element['frequency']['#default_value'] = $element['#value']['frequency'];
        }

        // Hide frequency selection if one or no options.
        if (count($element['frequency']['#options']) <= 1) {
          $element['frequency']['#access'] = FALSE;
        }

        $element['#element_validate'] = [
          [
            get_called_class(),
            'validateWebformDonateAmount',
          ],
        ];

        // Process amount element into container with amount selection for each
        // frequency type.
        $element['amount']['#type'] = 'container';
        // Added as breaks (core) numberValidation() otherwise.
        $element['amount']['#value'] = '';

        foreach ($amounts as $type_key => $type_details) {
          $amount_defaults = $amount_defaults_all[$type_key];

          // Wrap amount selector element in container - theme_wrappers did not work.
          $element['amount'][$type_key] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'webform_donate_amount-' . $type_key,
                'webform_donate_amount_container',
              ],
              'style' => 'display: none',
            ],
          ];

          if (!empty($type_details['amounts'])) {
            // Add an amount selection element for each frequency - use JS to
            // switch between them.
            $element['amount'][$type_key]['amounts'] = [
              '#type' => 'webform_buttons',
              '#title' => $element['amount']['#title'],
              '#default_value' => $amount_defaults['default_amount'],
              '#after_build' => [
                [self::class, 'afterBuildWebformOther'],
              ],
            ] + WebformDonateAmount::getElementProperties($type_details['amounts'], 'benefit');

            // Support other amount field.
            if (!empty($type_details['allow_other_amount'])) {
              // Use own other amount element to sort out validation issues.
              $element['amount'][$type_key]['amounts']['#type'] = 'webform_donate_amount_buttons_other';

              // Use own own text field to get input mask and min validation.
              $element['amount'][$type_key]['amounts']['#other__type'] = 'webform_donate_amount_textfield';

              // Use amount sub-element placeholder.
              $element['amount'][$type_key]['amounts']['#other__placeholder'] = !empty($element['#amount__placeholder']) ? $element['#amount__placeholder'] : t('Enter amount');

              // Set input mask - see https://github.com/RobinHerbots/Inputmask.
              // Input mask moved to webform_donate_amount_form.element.amount.js.
              if (!empty($type_details['minimum_amount'])) {
                $element['amount'][$type_key]['amounts']['#other__min'] = $type_details['minimum_amount'];
              }
            }

            // If value already set, then need to preserve in replacement
            // elements.
            if ($element['#value']['frequency'] == $type_key) {
              $element['amount'][$type_key]['amounts']['#default_value'] = $element['#value']['amount'];
            }
          }

          if ($handler->configuration[$type_key]['hero_image']['enabled'] && !empty($handler->configuration[$type_key]['hero_image']['image_reference'])) {
            $media_entity = Media::load($handler->configuration[$type_key]['hero_image']['image_reference']);
            $element[$type_key]['hero_image'] = [
              '#theme' => 'image_style',
              '#style_name' => $handler->configuration[$type_key]['hero_image']['image_style'],
              '#uri' => $media_entity->field_image->entity->getFileUri(),
              '#theme_wrappers' => [
                'container' => [
                  '#attributes' => ['class' => 'webform-donate-elements-hero-image'],
                ],
              ],
              '#weight' => 10,
            ];
          }
        }

        // Don't hide the default options.
        $default_type_key = $element['frequency']['#default_value'];
        unset($element['amount'][$default_type_key]['#attributes']['style']);

        // Add a benefit message element.
        $element['message'] = [
          '#type' => 'webform_markup',
          '#theme_wrappers' => [
            'container' => [
              '#attributes' => ['class' => 'webform_donate_elements_benefit'],
            ],
          ],
          '#markup' => '',
          '#weight' => -1,
        ];

        $element['#attached']['library'][] = 'webform_donate_elements.amount';
      }
    }

    return $element;
  }

  /**
   * Processes an 'other' element.
   *
   * See select list webform element for select list properties.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   The element array.
   *
   * @see \Drupal\Core\Render\Element\Select
   */
  public static function afterBuildWebformOther(array $element, FormStateInterface $form_state) {

    if ($element['#type'] == 'webform_buttons') {
      $buttons_element =& $element;
    }
    else {
      $buttons_element =& $element['buttons'];
    }

    foreach (Element::children($buttons_element) as $button_key) {
      if (!empty($element['#button_descriptions'][$button_key])) {
        $buttons_element[$button_key]['#description'] = $element['#button_descriptions'][$button_key];
      }
    }

    return $element;
  }

  /**
   * Get element properties from a settings array.
   *
   * @param array $settings
   *   Settings array.
   * @param string $description_key
   *   Key.
   *
   * @return array
   *   Return element properties array.
   */
  private static function getElementProperties(array $settings, $description_key = '') {
    $options = [];
    $descriptions = [];

    if (!empty($settings)) {
      $default_key = array_keys($settings)[0];
      foreach ($settings as $key => $details) {
        $options[$key] = $details['label'];

        if ($description_key) {
          $descriptions[$key] = !empty($details[$description_key]) ? $details[$description_key] : '';
        }
      }
    }

    return [
      '#options' => $options,
      '#button_descriptions' => $descriptions,
    ];
  }

  /**
   * Validates a webform_donate_amount element.
   */
  public static function validateWebformDonateAmount(&$element, FormStateInterface $form_state, &$complete_form) {
    // Donation webform amount field must be converted into a single value.
    $frequency = $element['frequency']['#value'] ?: $element['frequency']['#default_value'];

    $frequency = trim($frequency);

    $form_state->set('frequency', $frequency);

    if (!empty($element['amount'][$frequency]['amounts']['#parents'])) {
      $amount = NestedArray::getValue($form_state->getValues(), $element['amount'][$frequency]['amounts']['#parents']);
      $other_amount = $element['amount'][$frequency]['amounts']['other']['#value'];

      $amount = $other_amount ?: $amount;

      $form_state->setValueForElement($element['amount'], $amount);
      $form_state->set('amount', $amount);
    }

    return $element;
  }

}
