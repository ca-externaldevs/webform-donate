/**
 * @file
 * Javascript behaviors for amount element.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attach handlers to buttons other elements.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformDonateElementsButtonsAmount = {
    attach: function (context) {
      $('.webform-buttons-other-input input, .webform_donate_amount_other_separate_container input').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
      })
    }
  };

})(jQuery, Drupal);
